from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

ma = Marshmallow()
db = SQLAlchemy()

#modelo usuario
class Usuario(db.Model):
    __tablename__ = 'usuario'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(255), nullable=True)
    password = db.Column(db.Text(), nullable=True)
    correo = db.Column(db.String(255), nullable=True)
    createdAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updatedAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())

    def __init__(self, nombre, password, correo):
        self.nombre = nombre
        self.password = password
        self.correo = correo

class Cuarto(db.Model):
    __tablename__ = 'cuarto'
    id = db.Column(db.Integer, primary_key=True)
    uso = db.Column(db.String(255), nullable=True)
    dimension = db.Column(db.String(255), nullable=True)
    piso = db.Column(db.Integer, nullable=True)
    fid_usuario = db.Column(db.Integer, db.ForeignKey('usuario.id', ondelete='CASCADE'), nullable=False)
    usuario_fk = db.relationship('Usuario', backref=db.backref('cuarto', lazy='dynamic' ))
    createdAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updatedAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())

    def __init__(self, uso, dimension, piso, fid_usuario):
        self.uso = uso
        self.dimension = dimension
        self.piso = piso
        self.fid_usuario = fid_usuario

class DispositivoElectrico(db.Model):
    __tablename__ = 'dispositivo_electrico'
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(255), nullable=True)
    estado = db.Column(db.String(255), nullable=True)
    fid_cuarto = db.Column(db.Integer, db.ForeignKey('cuarto.id', ondelete='CASCADE'), nullable=False)
    cuarto_fk = db.relationship('Cuarto', backref=db.backref('dispositivo_electrico', lazy='dynamic' ))
    createdAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
    updatedAt = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False, onupdate=db.func.now())

    def __init__(self, descripcion, estado, fid_cuarto):
        self.descripcion = descripcion
        self.estado = estado
        self.fid_cuarto = fid_cuarto


class UsuarioSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    nombre = fields.String(required=True)
    password = fields.String(required=True)
    correo = fields.String()
    createdAt = fields.DateTime()
    updatedAt = fields.DateTime()

class CuartoSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    uso = fields.String()
    dimension = fields.String()
    piso = fields.Integer()
    fid_usuario = fields.Integer()
    createdAt = fields.DateTime()
    updatedAt = fields.DateTime()

class DispositivoElectricoSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    descripcion = fields.String()
    estado = fields.String()
    fid_cuarto = fields.Integer()
    createdAt = fields.DateTime()
    updatedAt = fields.DateTime()
