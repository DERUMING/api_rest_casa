from flask import request
from flask_restful import Resource
from Model import db, DispositivoElectrico, DispositivoElectricoSchema
#from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)

dispositivos_schema = DispositivoElectricoSchema(many=True)
dispositivo_schema = DispositivoElectricoSchema()

class DispositivoElectricoResource(Resource):
    #@jwt_required
    def get(self, id=None):
        if not id:
            dispositivos = DispositivoElectrico.query.all()
            dispositivos = dispositivos_schema.dump(dispositivos)
            return {'status': 200, 'message': 'Peticion correcta', 'data': dispositivos}, 200
        else:
            dispositivo = DispositivoElectrico.query.filter_by(id=id).first()
            if not dispositivo:
                return {'status': 400, 'message': 'Peticion incorrecta', 'data': 'Dispositivo no existe' }, 400
            result = dispositivo_schema.dump(dispositivo)
            return {'status': 200, 'message': 'Peticion correcta', 'data': result }, 200

    #@jwt_required
    def post(self):
        json_data = request.get_json(force=True)

        dispositivo = DispositivoElectrico(
            descripcion=json_data['descripcion'],
            estado=json_data['estado'],
            fid_cuarto=json_data['fid_cuarto']
            )
        db.session.add(dispositivo)
        db.session.commit()

        resultado = dispositivo_schema.dump(dispositivo)
        return { "status": 200, 'message': 'Peticion correcta', 'data': resultado }, 200

    #@jwt_required
    def put(self, id = None):
        json_data = request.get_json(force=True)

        dispositivo = DispositivoElectrico.query.filter_by(id=id).first()
        if not dispositivo:
            return {'status': 400, 'message': 'Peticion incorrecta', 'data': 'Dispositivo no existe' }, 400
        #actualizando recurso
        dispositivo.descripcion = json_data['descripcion']
        dispositivo.estado = json_data['estado']
        dispositivo.fid_cuarto = json_data['fid_cuarto']
        db.session.commit()
        result = dispositivo_schema.dump(dispositivo)
        return { "status": 200, 'message': 'Peticion correcta', 'data': result }, 200

    #@jwt_required
    def delete(self, id = None):
        # Eliminando recurso de la base de datos
        dispositivo = DispositivoElectrico.query.filter_by(id=id).delete()
        db.session.commit()

        return { "status": 204, 'message': 'Peticion correcta', 'data': {} }, 200
