from flask import request
from flask_restful import Resource
from Model import db, Usuario, UsuarioSchema
from passlib.hash import pbkdf2_sha256 as sha256
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)

usuarios_schema = UsuarioSchema(many=True)
usuario_schema = UsuarioSchema()

class UsuarioResource(Resource):
    @jwt_required
    def get(self, id=None):
        if not id:
            usuarios = Usuario.query.all()
            usuarios = usuarios_schema.dump(usuarios)
            return {'status': 200, 'message': 'Peticion correcta', 'data': usuarios}, 200
        else:
            usuario = Usuario.query.filter_by(id=id).first()
            if not usuario:
                return {'status': 400, 'message': 'Peticion incorrecta', 'data': 'Usuario no existe' }, 400
            resultado = usuario_schema.dump(usuario)
            return { 'status': 200, 'message': 'Peticion correcta', 'data': resultado }, 200

    def post(self):
        json_data = request.get_json(force=True)

        usuario = Usuario.query.filter_by(nombre=json_data['nombre']).first()
        if usuario:
            return {'status': 400, 'message': 'Usuario ya existe'}, 400
        usuario = Usuario(
            nombre=json_data['nombre'],
            password=sha256.hash(json_data['password']),
            correo=json_data['correo']
            )

        db.session.add(usuario)
        db.session.commit()
        access_token = create_access_token(identity = json_data['nombre'])
        refresh_token = create_refresh_token(identity = json_data['nombre'])
        resultado = usuario_schema.dump(usuario)

        return { 'status': 201, 'data': resultado, 'message': 'Peticion correcta', 'access_token': access_token, 'refresh_token': refresh_token }, 201


    def put(self, id = None):
        json_data = request.get_json(force=True)

        usuario = Usuario.query.filter_by(id=id).first()
        if not usuario:
            return {'status': 400, 'message': 'Peticion incorrecta', 'data': 'Usuario no existe' }, 400
        usuario.nombre = json_data['nombre']
        usuario.password = sha256.hash(json_data['password'])
        usuario.correo = json_data['correo']
        db.session.commit()

        resultado = usuario_schema.dump(usuario)

        return { 'status': 200, 'data': resultado, 'message': 'Peticion correcta' }, 200


    def delete(self, id = None):
        usuario = Usuario.query.filter_by(id=id).delete()
        db.session.commit()

        return { "status": 204, 'message': 'Peticion correcta', 'data': {} }, 200


class Login_usuarioResource(Resource):

    def post(self):
        json_data = request.get_json(force=True)

        usuario = Usuario.query.filter_by(nombre=json_data['nombre']).first()
        if not usuario:
            return {'message': 'Usuario no existe'}, 400
        if sha256.verify(json_data['password'], usuario.password):
            access_token = create_access_token(identity = json_data['nombre'])
            refresh_token = create_refresh_token(identity = json_data['nombre'])
            return {'message': 'Logeado con {}'.format(usuario.nombre), 'access_token': access_token, 'refresh_token': refresh_token }, 200
        else:
            return {'message': 'Credenciales incorrectas...'}, 400

class TokenRefreshResource(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}
