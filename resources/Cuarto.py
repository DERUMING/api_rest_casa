from flask import request
from flask_restful import Resource
from Model import db, Cuarto, CuartoSchema
#from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)

cuartos_schema = CuartoSchema(many=True)
cuarto_schema = CuartoSchema()

class CuartoResource(Resource):
    #@jwt_required
    def get(self, id=None):
        if not id:
            cuartos = Cuarto.query.all()
            cuartos = cuartos_schema.dump(cuartos)
            return {'status': 200, 'message': 'Peticion correcta', 'data': cuartos}, 200
        else:
            cuarto = Cuarto.query.filter_by(id=id).first()
            if not cuarto:
                return {'status': 400, 'message': 'Peticion incorrecta', 'data': ['Cuarto no existe'] }, 400
            result = cuarto_schema.dump(cuarto)
            return { 'status': 200, 'message': 'Peticion correcta', 'data': result }, 200

    #@jwt_required
    def post(self):
        json_data = request.get_json(force=True)

        cuarto = Cuarto(
            uso=json_data['uso'],
            dimension=json_data['dimension'],
            piso=json_data['piso'],
            fid_usuario=json_data['fid_usuario']
            )
        db.session.add(cuarto)
        db.session.commit()

        result = cuarto_schema.dump(cuarto)
        return { "status": 200, 'message': 'Peticion correcta', 'data': result }, 200

    #@jwt_required
    def put(self, id = None):
        json_data = request.get_json(force=True)

        cuarto = Cuarto.query.filter_by(id=id).first()
        if not cuarto:
            return {'status': 400, 'message': 'Peticion incorrecta', 'data': 'Cuarto no existe' }, 400
        #actualizando recurso
        cuarto.uso = json_data['uso']
        cuarto.dimension = json_data['dimension']
        cuarto.piso = json_data['piso']
        cuarto.fid_usuario = json_data['fid_usuario']
        db.session.commit()
        result = cuarto_schema.dump(cuarto)
        return { "status": 200, 'message': 'Peticion correcta', 'data': result }, 200

    #@jwt_required
    def delete(self, id = None):
        # Eliminando recurso de la base de datos
        cuarto = Cuarto.query.filter_by(id=id).delete()
        db.session.commit()

        return { "status": 204, 'message': 'Peticion correcta', 'data': {} }, 200
