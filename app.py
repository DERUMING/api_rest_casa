from flask import Blueprint
from flask_restful import Api
from resources.Hello import Hello
from resources.Usuario import UsuarioResource, Login_usuarioResource, TokenRefreshResource
from resources.Cuarto import CuartoResource
from resources.DispositivoElectrico import DispositivoElectricoResource
api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Rutas
api.add_resource(Hello, '/Hello')
api.add_resource(UsuarioResource, '/Usuario', '/Usuario/<int:id>')
api.add_resource(CuartoResource, '/Cuarto', '/Cuarto/<int:id>')
api.add_resource(DispositivoElectricoResource, '/Dispositivo', '/Dispositivo/<int:id>')
api.add_resource(TokenRefreshResource, '/Actualizar_token')
api.add_resource(Login_usuarioResource, '/Login')
