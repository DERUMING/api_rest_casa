from flask import Flask
from flask_jwt_extended import JWTManager
##from flask_swagger_ui import get_swaggerui_blueprint
from flask_cors import CORS

def create_app(config_filename):
    app = Flask(__name__)
    ### swagger specific ###
    ##SWAGGER_URL = '/swagger'
    ##API_URL = '/static/swagger.json'
    ##SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    ##    SWAGGER_URL,
    ##    API_URL,
    ##    config={
    ##        'app_name': "Seans-Python-Flask-REST-Boilerplate"
    ##    }
    ##)
    ##app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
    CORS(app)
    app.config.from_object(config_filename)
    #configuraicon para JWT
    jwt = JWTManager(app)
    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/api/v1')

    from Model import db
    db.init_app(app)

    return app

if __name__ == "__main__":
    app = create_app("config")
    app.run(debug=True)
