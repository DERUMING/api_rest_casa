# API REST CASA v.1 con Flask

Esta es la primera version de un Api Rest basado en Python y Flask, fue realizado para el curso taller en la carrera de Ingenieria de Sistemas de la UPEA.

## Comenzando 🚀   
_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

_Requerimientos minimos del sistema._

1. Gestor de Base de datos PostgreSQL o MySQL.
2. Python3.x
  ````
  $ sudo apt install python3
  ````
3. virtualenv.
  ````
  $ pip install virtualenv
  ````

### Instalación 🔧

_Sigue los siguientes pasos despues de tener instalado y funcionando los requisitos mensionados enteriormente._

Descarga el proyecto desde el repositorio, en un directorio de tu elección.


Dentro del directorio de tu proyecto ejecuta los siguientes comandos para crear un entorno virtual y activarlo.

```
#para crear un entorno virtual
$ virtualenv env
#para activar el entorno virtual
$ source env/bin/activate
#en windows se activa ejecutando el siguiente comando
$ env\Script\activate.bat
```
Despues de activar el entorno virtual, ejecuta el siguiente comando para instalar las librerias requeridas para el funcionamiento del api.

```
$ pip install -r requirements.txt
```

Crea un usuario y una base de datos ejecutando los siguientes comandos.
````
postgres=# create user python2 with password 'python2';
postgres=# create database prueba_db2 with owner python2;
````
Despues de crear la base de datos, para crear el esquema dentro de la base de datos, ejecuta los siguientes comandos.
````
$ python3 migrate.py db init
````
````
$ python3 migrate.py db migrate
````
````
$ python3 migrate.py db upgrade
````
Puedes verificar que el esquema se haya generado ejecutando los siguientes comando dentro de la consola de PostgreSQL.
````
postgres=# \c prueba_db2
prueba_db2=# \d
````
Finalmente tienes listo el Api Rest para su funcionamiento y consumo, para poner en marcha el servidor ejecuta el siguiente comando.
````
$ python3 run.py
* Serving Flask app "run" (lazy loading)
* Environment: production
  WARNING: This is a development server. Do not use it in a production deployment.
  Use a production WSGI server instead.
* Debug mode: on
* Restarting with stat
* Debugger is active!
* Debugger PIN: 110-262-037
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
````
## Ejecutando las pruebas ⚙️

Para observar el despliegue del sistema, ingresa el siguiente enlace http://localhost:5000/api/v1/Hello en tu navegador, deberias obtener la siguiente respuesta.
````
{
    "message": "Hello, World!"
}
````
Para consumir los servicios del api rest te recomiendo que tengas instalado [Postman](https://www.getpostman.com/).


El Api rest te da acceso a los recursos mediante los siguientes en laces.
````
http://localhost:5000/api/v1/Usuario
http://localhost:5000/api/v1/Cuarto
http://localhost:5000/api/v1/Dispositivo
http://localhost:5000/api/v1/Hello

````

## Construido con 🛠️

* [Python]()
* [Flask]()
* [PostgreSQL]()

## Autores ✒️

* **Rolando Ulo Mollo** - *Trabajo Inicial*

## Expresiones de Gratitud 🎁

* Agradecido con la UPE.

⌨️ por [derum](https://www.facebook.com/rolando.ulorumsisetn) 😊
