import os
# Remplaza los siguientes valores con los datos de tu configuaración
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
JWT_SECRET_KEY = 'jwt-secret-string'
#Configuracion para base de datos mysql o mariadb
##SQLALCHEMY_DATABASE_URI = "mysql://root:''@localhost/db_hogar"
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:''@localhost:3306/db_hogar'
#Configuracion para base de datos postgresql
#SQLALCHEMY_DATABASE_URI = "postgresql://derum:derum@localhost/holamundo_db"
